<?php
// src/tests/CarreTest.php
namespace Coco\Redaproject;

use PHPUnit\Framework\TestCase;
use Exception;

class CarreTest extends TestCase
{
    public function testSurface()
    {
        $objet = new Carre(10);
        $this->assertEquals(100, $objet->surface());
    }
    public function testSetCote()
    {
        $this->expectException(Exception::class);
        $objet = new Carre(-23);
        $objet->setCote(-20);
    }
}
